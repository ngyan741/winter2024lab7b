public class Card{
	
	//establish suit and value fields with the enums
	Suit suit;
	Value value;
	
	//constructor that takes in two enum inputs 
	public Card(Suit suit, Value value){
		this.suit = suit;
		this.value = value;
	}
	
	//toString method
	public String toString(){
		return this.value.getValueName() + " of " + this.suit.getSuitName();
	}
	
	//method that returns the score of the card based on it's value and suit, uses the getters from the enums to get the score values and adds them. The score is returned as a double.
	public double calculateScore(){
		double score = this.suit.getSuitScore() + this.value.getValueScore();
		return score;
	}
}