public class SimpleWar{
	public static void main (String args[]){
		//create the game's deck, shuffle it, and create variables for the players' points
		Deck warDeck = new Deck();
		warDeck.shuffle();
		int p1Points = 0;
		int p2Points = 0;
		
		//main game loop that will continue to run until deck runs out of cards
		while(warDeck.length() > 0){
			//print current points
			System.out.println("Player 1 Points: " + p1Points);
			System.out.println("Player 2 Points: " + p2Points);
			System.out.println("*********************************");
			//draw cards for each player and store it
			Card p1Card = warDeck.drawTopCard();
			Card p2Card = warDeck.drawTopCard();
			//calculate the score of the cards and store them
			double p1CardScore = p1Card.calculateScore();
			double p2CardScore = p2Card.calculateScore();
			//print out the player's card and it's score
			System.out.println("Player 1's Card: " + p1Card + "\n" + p1CardScore);
			System.out.println("Player 2's Card: " + p2Card + "\n" + p2CardScore);
			System.out.println("*********************************");
			//determine which card is larger and print who won this round, and give them a point
			if(p1CardScore > p2CardScore){
				System.out.println("Player 1 wins this round!");
				p1Points++;
			}
			else{
				System.out.println("Player 2 wins this round!");
				p2Points++;
			}
			//print current points
			System.out.println("Player 1 Points: " + p1Points);
			System.out.println("Player 2 Points: " + p2Points);
			System.out.println("-----------------------------------------------");
		}
		//congratulate whoever has the most points at the end
		if(p1Points > p2Points){
			System.out.println("Player 1 wins the game! Congratulations!");
		}
		else if(p2Points > p1Points){
			System.out.println("Player 2 wins the game! Congratulations!");
		}
		else{
			System.out.println("It's a tie!");
		}
	}
}