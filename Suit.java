public enum Suit{
	//suit enum values with two fields: their respective scores and the name in lowercase
	HEARTS(0.4, "Hearts"),
	DIAMONDS(0.3, "Diamonds"),
	CLUBS(0.2, "Clubs"),
	SPADES(0.1, "Spades");
	
	//creating the fields
	private double suitScore;
	private String suitName;
	
	//constructor (cannot be public or protected)
	Suit(double suitScore, String suitName){
		this.suitScore = suitScore;
		this.suitName = suitName;
	}
	
	//getters
	public double getSuitScore(){
		return this.suitScore;
	}
	
	public String getSuitName(){
		return this.suitName;
	}
}