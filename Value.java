public enum Value{
	//value enum values with two fields: their respective scores and the name in lowercase
	ACE(1.0, "Ace"),
	TWO(2.0, "Two"),
	THREE(3.0, "Three"),
	FOUR(4.0, "Four"),
	FIVE(5.0, "Five"),
	SIX(6.0, "Six"),
	SEVEN(7.0, "Seven"),
	EIGHT(8.0, "Eight"),
	NINE(9.0, "Nine"),
	TEN(10.0, "Ten"),
	JACK(11.0, "Jack"),
	QUEEN(12.0, "Queen"),
	KING(13.0, "King");
	
	//creating the fields
	private double valueScore;
	private String valueName;
	
	//constructor (cannot be public or protected)
	private Value(double valueScore, String valueName){
		this.valueScore = valueScore;
		this.valueName = valueName;
	}
	
	//getters
	public double getValueScore(){
		return this.valueScore;
	}
	
	public String getValueName(){
		return this.valueName;
	}
}